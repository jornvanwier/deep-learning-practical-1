import torch.nn.functional as F
from torch import nn


class Classifier(nn.Module):
    """
    Adapted from https://www.kaggle.com/paultimothymooney/identify-blood-cell-subtypes-from-images
    """

    FULLY_CONNECTED_SIZE = 64 * 5 * 5
    USE_DROPOUT = True
    USE_BN = False

    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 32, 3)
        self.conv2 = nn.Conv2d(32, 64, 3)
        if self.USE_BN:
            self.bn = nn.BatchNorm2d(64)
        if self.USE_DROPOUT:
            self.dropout1 = nn.Dropout2d(.25)
        self.adapt_pool = nn.AdaptiveMaxPool2d(5)
        self.fc1 = nn.Linear(self.FULLY_CONNECTED_SIZE, 128)
        self.dropout2 = nn.Dropout(.5)
        self.fc2 = nn.Linear(128, 5)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        if self.USE_BN:
            x = self.bn(x)
        if self.USE_DROPOUT:
            x = self.dropout1(x)
        x = self.adapt_pool(x)
        x = x.flatten(start_dim=1)
        x = F.relu(self.fc1(x))
        x = self.dropout2(x)
        x = self.fc2(x)
        return x
