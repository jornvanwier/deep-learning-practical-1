import os
import random
from enum import Enum
from pathlib import Path
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import io, transforms


class CellType(Enum):
    EOSINOPHIL = 0
    LYMPHOCYTE = 1
    MONOCYTE = 2
    NEUTROPHIL = 3


class BCCDDataset(Dataset):
    def __init__(self, device: torch.device, label_path: Path, image_root: Path, extension: str = '.jpeg') -> None:
        self.device = device

        self.labels = pd.read_csv(label_path)
        self.image_paths = [
            file
            for files in (
                [Path(root).joinpath(file) for file in files if file.endswith(extension)]
                for root, _, files
                in os.walk(image_root)
            )
            if any(files)
            for file in files
            if self.label_from_filename(file) is not None
        ]

        # TODO shift
        # TODO only use random transforms for test loader
        random_transforms = [
            transforms.RandomRotation(10),
            transforms.RandomHorizontalFlip(),
        ]

        # Image base size 240 x 320
        self.transform = transforms.Compose([
            transforms.ConvertImageDtype(torch.float),
            *random_transforms,
            transforms.Normalize((.5, .5, .5), (.5, .5, .5)),
        ])

    def __len__(self) -> int:
        return len(self.image_paths)

    def __getitem__(self, index) -> Tuple[torch.Tensor, CellType]:
        file = self.image_paths[index]
        label = self.label_from_filename(file)

        if label is None:
            raise ValueError('None label found, filter the dataset')

        img = io.read_image(str(file)).to(self.device)
        img = self.transform(img)

        return img, torch.tensor(label.value).to(self.device)

    @staticmethod
    def label_from_filename(file: Path) -> CellType:
        # Get directory name
        label_name = file.parts[-2]
        return CellType[label_name]


def create_dataset(device: torch.device, train: bool) -> BCCDDataset:
    base_path = Path('../data')
    if train:
        image_path = base_path.joinpath('images', 'TRAIN')
    else:
        image_path = base_path.joinpath('images', 'TEST')

    return BCCDDataset(device, base_path.joinpath('labels.csv'), image_path)


def get_loader(ds: BCCDDataset, shuffle: bool) -> DataLoader:
    return DataLoader(ds, batch_size=32, shuffle=shuffle, num_workers=0)


def get_datasets(device: torch.device, val_percentage: float = .2) -> Tuple[DataLoader, DataLoader, DataLoader]:
    train_ds = create_dataset(device, train=True)
    test_ds = create_dataset(device, train=False)

    n_train = len(train_ds)
    n_val = int(np.floor(n_train * val_percentage))

    train_split, val_split = torch.utils.data.random_split(train_ds, [n_train - n_val, n_val])
    return (
        get_loader(train_split, shuffle=True),
        get_loader(val_split, shuffle=False),
        get_loader(test_ds, shuffle=False),
    )


def main():
    ds = BCCDDataset(torch.device('cuda'), Path('../data/labels.csv'), Path('../data/images/TEST'))
    im, lbl = ds[random.randint(0, len(ds) - 1)]
    plt.imshow(im.permute(1, 2, 0))
    plt.title(CellType(lbl).name)
    plt.show()


if __name__ == '__main__':
    main()
