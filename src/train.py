import csv
import os
import sys
from typing import Tuple, Optional

import matplotlib.pyplot as plt
import torch
from torch import nn, optim
from torch.utils.data import DataLoader
from tqdm import tqdm

import data
from model import Classifier

MODEL_PATH = '../model.pth'
RESULTS_PATH = '../results.csv'
LOG_TEST = False


def eval_model(model: Classifier, data_loader: DataLoader, category: str, report_class_acc: bool = False) -> float:
    model.eval()

    n_correct = 0

    class_correct = list(0 for _ in data.CellType)
    class_total = list(0 for _ in data.CellType)

    # noinspection PyTypeChecker
    val_length = len(data_loader.dataset)

    with torch.no_grad():
        for (x, y) in tqdm(data_loader, category, file=sys.stdout):
            outputs = model(x)
            _, pred = torch.max(outputs.data, 1)
            correct: torch.Tensor = (pred == y).squeeze()
            n_correct += correct.sum().item()
            if report_class_acc:
                for class_pred, class_label in zip(correct, y):
                    class_correct[class_label] += class_pred.item()
                    class_total[class_label] += 1

    accuracy = n_correct / val_length
    tqdm.write(f'{category} accuracy: {(100 * accuracy):.2f}%')

    if report_class_acc:
        for c in data.CellType:
            class_accuracy = class_correct[c.value] / class_total[c.value]
            tqdm.write(f'{category} accuracy for {c.name}: {100 * class_accuracy:.2f}%')

    return accuracy


def train(model: Classifier, optimizer: optim.Optimizer, datasets: Tuple[DataLoader, DataLoader, DataLoader]) -> \
        Optional[float]:
    train_data, val_data, test_data = datasets

    epochs = 25
    early_stop = .85
    early_stop_reached = None

    loss_fn = nn.CrossEntropyLoss()

    # noinspection PyTypeChecker
    n_training_samples = len(train_data.dataset)
    acc_history = {
        'Train': [],
        'Validation': [],
    }
    if LOG_TEST:
        acc_history['Test'] = []

    best_val_acc = 0.
    best_epoch = None

    try:
        for epoch in range(1, epochs + 1):
            model.train()

            running_loss = 0.
            running_correct = 0

            for i, (x, y) in enumerate(tqdm(train_data, f'Train epoch {epoch}/{epochs}', file=sys.stdout)):
                optimizer.zero_grad()

                outputs = model(x)
                _, pred = torch.max(outputs, 1)
                loss = loss_fn(outputs, y)

                running_loss += loss.item() * x.size(0)
                running_correct += (pred == y).sum().item()

                loss.backward()
                optimizer.step()

            epoch_acc = running_correct / n_training_samples
            acc_history['Train'].append(epoch_acc)
            epoch_loss = running_loss / n_training_samples
            tqdm.write(f'Epoch finished: loss {epoch_loss:.2f}, accuracy {(100 * epoch_acc):.2f}%')

            val_acc = eval_model(model, val_data, 'Validation', report_class_acc=True)

            if val_acc > best_val_acc:
                best_val_acc = val_acc
                best_epoch = epoch
                tqdm.write('Model improved, saving.')
                torch.save(model.state_dict(), MODEL_PATH)

            acc_history['Validation'].append(val_acc)

            if LOG_TEST:
                acc_history['Test'].append(eval_model(model, test_data, 'Test'))

            for name, history in acc_history.items():
                plt.plot(history, label=name)
            plt.legend()
            plt.title(f'Accuracy epoch {epoch}')
            plt.show()

            if early_stop_reached is None and val_acc >= early_stop:
                tqdm.write(f'Reached {100 * early_stop:.2f} accuracy after {epoch} epochs.')
                early_stop_reached = epoch

            tqdm.write('')

    except KeyboardInterrupt:
        tqdm.write('Intercepted KeyboardInterrupt, training aborted.')

    tqdm.write(f'Finished training, best epoch was {best_epoch}.')
    return early_stop_reached


def test(model: Classifier, test_dl: DataLoader) -> float:
    return eval_model(model, test_dl, 'Test', report_class_acc=True)


def main() -> None:
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    print('Using device', device.type)

    train_ds, val_ds, test_ds = data.get_datasets(device)

    model = Classifier().to(device)

    optimizer = optim.Adam(model.parameters(), lr=.001)
    # optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)

    early_stop_epoch = train(model, optimizer, (train_ds, val_ds, test_ds))

    # Reload best model
    model.load_state_dict(torch.load(MODEL_PATH))
    test_acc = test(model, test_ds)

    optimizer_name = type(optimizer).__name__
    print('Completed training.')
    print('Run configuration:')
    print({
        'optimizer': optimizer_name,
        'bn': model.USE_BN,
        'dropout': model.USE_DROPOUT,
    }, '\n')
    print('Early stop:', early_stop_epoch)
    print('Test accuracy:', test_acc)

    new_file = not os.path.exists(RESULTS_PATH)

    with open(RESULTS_PATH, 'a') as f:
        writer = csv.writer(f)

        if new_file:
            writer.writerow(['Optimizer', 'BN', 'Dropout', 'Early stop', 'Final acc'])

        writer.writerow([optimizer_name, model.USE_BN, model.USE_DROPOUT, early_stop_epoch, test_acc])


if __name__ == '__main__':
    main()
