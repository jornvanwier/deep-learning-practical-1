# Classifying Blood Cell images
In this experiment we aim to classify blood cell images using a Convolutional Neural network (CNN). Regularization techniques and optimizers play a large role in how fast a network converges, and whether it converges to optimal performance. We created a simple CNN and tested various combinations of regularization techniques such as Dropout and Batch normalization together with optimizers Stochastic Gradient Descent and Adam. We found in the end that Adam generally outperformed SGD, Batch normalization and Dropout should not be used together, and Batch normalization could replace Dropout in many cases.

Our model is implemented using PyTorch.
